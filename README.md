# lambda-shutdown-resources

This lambda shuts down resources to save costs.

* sagemaker notebooks every evening at 18h UTC

## Requirements

* npm
* serverless (npm i -g serverless)
* python3.8+
* AWS credentials setup to deploy from local environment

## Setup

Install dependencies in local python environment

```shell
$ python3 -m venv venv
$ source venv/bin/activate
$ pip3 install -r requirements.txt
```

Install serverless requirements

```shell
serverless plugin install -n serverless-python-requirements --stage <stage>
serverless plugin install -n serverless-plugin-utils --stage <stage>
serverless plugin install -n serverless-deployment-bucket --stage <stage>
```

## Package & deploy

Locally build and package the serverless function.

You need to provide the stage argument, choose your own (replace YYY) to not impact other developers.

```shell
serverless package --stage <stage>
```

Deploy in AWS, replace XXX profile name with your AWS profile with the credentials.

```shell
AWS_PROFILE="XXX" serverless deploy --stage <stage>
```

If successfully deployed, you should a CloudFormation stack with your resources deployed in AWS.