import os
from typing import Dict

from src.common.aws_clients import sagemaker_client
from src.common.aws_requests import get_ssm_parameter
from src.common.log import create_logger
from src.common.utils import get_env_var_value

os.environ["LOGGER_NAME"] = "shutdown-resources"
logger = create_logger(get_env_var_value("LOGGER_NAME"))


def shutdown_notebooks() -> Dict:
    client = sagemaker_client()
    result = dict()

    logger.info("Started retrieving running SageMaker notebooks.")
    response = client.list_notebook_instances(MaxResults=100, StatusEquals='InService')
    if "NotebookInstances" in response.keys():
        notebook_instances = response["NotebookInstances"]
        logger.info(f"Found {len(notebook_instances)} running SageMaker notebook.")
        if notebook_instances:
            for notebook_instance in notebook_instances:
                instance_name = notebook_instance['NotebookInstanceName']
                arn = notebook_instance["NotebookInstanceArn"]
                tags = client.list_tags(ResourceArn=arn)
                shutdown_protection_tag = False
                for tag in tags['Tags']:
                    key = tag["Key"]
                    value = tag["Value"]
                    if key == "shutdown-protection" and value == "true":
                        shutdown_protection_tag = True
                        logger.info(f"Found shutdown-protection flag set to true for the notebook '{instance_name}', "
                                    f"not shutting it down.")
                        break
                if not shutdown_protection_tag:
                    logger.info(f"Shutting down notebook {instance_name}")
                    client.stop_notebook_instance(NotebookInstanceName=instance_name)
                    result[instance_name] = "Shut down"
                else:
                    result[instance_name] = "Not shut down because of shutdown-protection tag."
        else:
            logger.info("Nothing to do for SageMaker notebooks.")
    return result


def delete_endpoints():
    client = sagemaker_client()
    endpoints_to_shutdown_ssm_path = [
        "/tedai/sagemaker/endpoint/budgetary_value_classifier/name"
    ]
    result = dict()

    logger.info(f"Started shutting down {len(endpoints_to_shutdown_ssm_path)}.")
    for endpoint_to_shutdown in endpoints_to_shutdown_ssm_path:
        endpoint_name = get_ssm_parameter(endpoint_to_shutdown)
        logger.info(f"Stopping {endpoint_name}")
        client.delete_endpoint(EndpointName=endpoint_name)
        result[endpoint_name] = "Deleted"
    return result


def handle(_event, _context) -> Dict:
    try:
        shutdown_notebooks_results = shutdown_notebooks()
        logger.info(f"Shutdown notebook results: {shutdown_notebooks_results}")

        delete_endpoints_results = delete_endpoints()
        logger.info(f"Delete endpoint results: {delete_endpoints_results}")

        return {"statusCode": 200, "body": f"Process complete."}
    except Exception as err:
        logger.error(f'Error during the execution of the resources shut down : {err}')
