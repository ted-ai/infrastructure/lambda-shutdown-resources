import logging

from src.common.aws_clients import ssm_client
from src.common.exceptions import GetSSMException
from src.common.utils import get_env_var_value


def get_ssm_parameter(path: str) -> str:
    logger = logging.getLogger(get_env_var_value("LOGGER_NAME"))
    try:
        parameter = ssm_client().get_parameter(Name=path)
        table_name = parameter['Parameter']['Value']
        logger.info(f"Parameter : '{path}' retrieved from SSM")
        return table_name
    except Exception as err:
        raise GetSSMException(f"Get SSM Exception when retrieving parameter : '{path}'."
                              f" Exception : {err}") from err
