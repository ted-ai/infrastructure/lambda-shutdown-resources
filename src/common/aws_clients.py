import boto3


def sagemaker_client():
    return boto3.client('sagemaker')


def ssm_client():
    return boto3.client('ssm')
