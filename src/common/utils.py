import os
from src.common.exceptions import MissingEnvironmentVariableException


def get_env_var_value(env_variable_name: str) -> str:
    try:
        return os.environ[env_variable_name]
    except KeyError as err:
        raise MissingEnvironmentVariableException(
                f"Missing environment variable '{env_variable_name}', process stopped.") from err
