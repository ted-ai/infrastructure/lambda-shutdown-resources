class MissingEnvironmentVariableException(Exception):
    def __init__(self, message: str):
        super().__init__(message)
        self.message = message


class GetSSMException(Exception):
    def __init__(self, message: str):
        super().__init__(message)
        self.message = message
